Feature: Get quote for gas powered vehicle
  Scenario: Gas is available within the dropdown
    Given Vehicle Model takes gas as fuel type
    When A user enters their vehicle model type
    Then Gas is an available option within the fuel type dropdown
