package com.glue;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(dryRun = false,
                    features = ".",
                    monochrome = false,
                    glue = "com/glue",
                    plugin = {"pretty", "html:target/cucumber-html-report", "json:target/cucumber-report.json"}
                )

public class RunTest {

}
